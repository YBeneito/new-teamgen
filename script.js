var tirer = document.getElementById("tirage");
var addStudent = document.getElementById("addStudent");

tirer.addEventListener("click", () => {
    getStudents((studs) => {
        var suffledList = shuffle(studs);
        var groups = generateGroups(suffledList);
        updateView(groups);
    });

});

document.getElementById("stu").addEventListener("click",(e) => {
    getStudent()

})

addStudent.addEventListener("click", (e) => {
    e.preventDefault()
    let newStudent = document.getElementById("firstname").value;
    addStudentToList(newStudent);
});