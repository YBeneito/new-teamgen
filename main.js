const students = ["tutu","toto","tete","tata"];

/**
 * Requête envoyant le om d'un nouvel étudiant à notre serveur
 * retourne le status de la requete une fois celle ci effectuée
 * @param {*} newStudent
 * @return {*} status 
 */

function addStudentToList(newStudent){
    let xhr = new XMLHttpRequest();
    xhr.open("POST","http://localhost:53443/student");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xhr.onload = () => {
        console.log(xhr.status);
    }
    
    xhr.send(`name=${newStudent}`)
}


/**
 * Récupére le nom d'un étudiant depuis le serveur.
 * grace au prompt, on demande l'index de la personne voulue.
 * pendant ce temps la requete pour récupérer tout le tableau 
 * est envoyée et on affiche l'index demandé du tableau
 * @returns req.status
 */
function getStudent(){
    let xhr = new XMLHttpRequest();
    xhr.open("GET","http://localhost:53443/students");
    xhr.readyState
    let askStu = window.prompt("Lequel ?")
    xhr.onload = ()=>{
        let studs = JSON.parse(xhr.responseText);
        let stu = studs[askStu]
        console.log(stu);       
    }
    xhr.send();
}



function getStudents(callback){
    let xhr = new XMLHttpRequest();
    xhr.open("GET","http://localhost:53443/students");
    xhr.readyState
    xhr.onload = ()=>{
        console.log(JSON.parse(xhr.responseText));
        callback(JSON.parse(xhr.responseText));
    }
    xhr.onerror = ()=>{
        callback(students);
    }
    xhr.send();
}

/** 
 * @param {*} list liste a scinder en deux
 * @returns {*} retourne la liste des groupes
*/
function generateGroups(list,nbGroup = 2) {
    let groups = [];
    let step = Math.floor(list.length / nbGroup);
    for(let start = 0;start<list.length;start=step){
        let group = [];
        step += start;
        if(step > list.length - 1){
            group = list.slice(start);
        }else{
            group = list.slice(start, step);
        }       
        groups.push(group);    
    }
    return groups;
}


/**
 * @param {*} list liste à melanger
 * @return {*} shuffledList liste mélangée
*/
function shuffle(list) {
    let shuffledList = list;
    let index1, index2;
    let nbPermutationsRestantes = Math.floor(Math.random() * 100 ) + list.length;

    while (nbPermutationsRestantes > 0) {
        // tirer aléatoirement deux index différents
        index1 = Math.floor(Math.random() * list.length);
        do {
            index2 = Math.floor(Math.random() * list.length);
        } while (index1 === index2);

        // permuter les deux valeurs 
        let temp = shuffledList[index1];
        shuffledList[index1] = shuffledList[index2];
        shuffledList[index2] = temp;


        nbPermutationsRestantes --;
    }
    return shuffledList;
}


/**
 * Mettre a jour la vue avec les groupes passés en argument
 * @param {*} groups liste des groupes a afficher
 */
function updateView(groups) {
    document.getElementById("content").innerHTML = "";
    for(i=0;i<groups.length;i++){
        let group = groups[i];
        let table = document.createElement("table");
        for(j=0;j<group.length;j++){
            let student = group[j];
            let tr = document.createElement("tr");
            let td = document.createElement("td");
            td.innerText = student;
            tr.appendChild(td);
            table.appendChild(tr);
        }        
        document.getElementById("content").appendChild(table);
    }
}